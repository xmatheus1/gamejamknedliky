﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts
{
    [Serializable]
    public class Triangle
    {
        public Vector2 A;
        public Vector2 B;
        public Vector2 C;
    }
}
