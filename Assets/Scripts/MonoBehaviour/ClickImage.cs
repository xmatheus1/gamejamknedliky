﻿using UnityEngine;

public class ClickImage : MonoBehaviour
{
    public ImageManager ImageManager;

    public float ResizeSpeed = 10f;
    private bool IsClicked = false;

    private Mesh Mesh;
    private Mesh PreviousMesh;

    public int Index;
    public GameObject Previous;
    public Texture Image;

    private void OnMouseDown()
    {
        IsClicked = true;
    }

    private void Start()
    {
        Mesh = GetComponent<MeshFilter>().mesh;
        PreviousMesh = Previous.GetComponent<MeshFilter>().mesh;
    }

    private void Update()
    {
        if (IsClicked)
        {
            MoveEdges();
        }
    }

    private void MoveEdges()
    {
        var newVertices = new Vector3[]
        {
            Vector3.MoveTowards(Mesh.vertices[0], ImageManager.CornerBottomLeft, Time.deltaTime * ResizeSpeed),
            Vector3.MoveTowards(Mesh.vertices[1], ImageManager.CornerBottomRight, Time.deltaTime * ResizeSpeed),
            Vector3.MoveTowards(Mesh.vertices[2], ImageManager.CornerTopLeft, Time.deltaTime * ResizeSpeed),
            Vector3.MoveTowards(Mesh.vertices[3], ImageManager.CornerTopRight, Time.deltaTime * ResizeSpeed)
        };




        var newVerticesPreviousImage = new Vector3[]
        {
            PreviousMesh.vertices[0] + (newVertices[0] - Mesh.vertices[0]),
            PreviousMesh.vertices[1] + (newVertices[1] - Mesh.vertices[1]),
            PreviousMesh.vertices[2] + (newVertices[2] - Mesh.vertices[2]),
            PreviousMesh.vertices[3] + (newVertices[3] - Mesh.vertices[3])
        };

        if ((Mathf.Abs(newVertices[0].x - Mesh.vertices[0].x) == 0 && Mathf.Abs(newVertices[0].y - Mesh.vertices[0].y) <= 0 )
            && (Mathf.Abs(newVertices[1].x - Mesh.vertices[1].x) == 0 && Mathf.Abs(newVertices[1].y - Mesh.vertices[1].y) <= 0)
            && (Mathf.Abs(newVertices[2].x - Mesh.vertices[2].x) == 0 && Mathf.Abs(newVertices[2].y - Mesh.vertices[2].y) <= 0)
            && (Mathf.Abs(newVertices[3].x - Mesh.vertices[3].x) == 0 && Mathf.Abs(newVertices[3].y - Mesh.vertices[3].y) <= 0))
        {
            newVertices = new Vector3[]
            {
                new Vector3(newVertices[0].x,newVertices[0].y,0),
                new Vector3(newVertices[1].x,newVertices[1].y,0),
                new Vector3(newVertices[2].x,newVertices[2].y,0),
                new Vector3(newVertices[3].x,newVertices[3].y,0),
            };

            var material = new Material(Shader.Find("Unlit/Color"));
            material.SetTexture("_MainTex", Image);
            GetComponent<MeshRenderer>().sharedMaterial = material;

            Mesh.vertices = newVertices;
            DestroyImmediate(Previous);
            ImageManager.SendNext(Index);
            DestroyImmediate(GetComponent<Collider>());
            DestroyImmediate(this);            
        }
        else
        {
            Mesh.vertices = newVertices;
            PreviousMesh.vertices = newVerticesPreviousImage;
        }       
    }
}
