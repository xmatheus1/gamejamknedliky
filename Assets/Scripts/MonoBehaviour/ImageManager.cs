﻿using Assets.Scripts;
using Assets.Scripts.Scriptable;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ImageManager : MonoBehaviour
{
    public Vector3 CornerTopRight;
    public Vector3 CornerTopLeft;
    public Vector3 CornerBottomRight;
    public Vector3 CornerBottomLeft;

    public LevelCollection AllLevels;

    private List<GameObject> LoadedLevels;

    void Start()
    {
        LoadedLevels = new List<GameObject>();

        LoadedLevels.Add(CreateBackground(AllLevels.Levels[0]));
        for (int i = 1; i < AllLevels.Levels.Count; i++)
        {
            LoadedLevels.Add(CreateTriangle(AllLevels.Levels[i], AllLevels.Levels[i - 1].Triangle, LoadedLevels.Last(), i));
        }

        for (int i = 2; i < LoadedLevels.Count; i++)
        {
            LoadedLevels[i].SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    private GameObject CreateBackground(Level level)
    {
        var backgroundGO = new GameObject();

        var mesh = new Mesh();
        mesh.vertices = new Vector3[] { CornerBottomLeft, CornerBottomRight, CornerTopLeft, CornerTopRight };
        mesh.triangles = new int[] { 0, 2, 1, 1, 2, 3 };
        mesh.SetUVs(0, new List<Vector2> { new Vector2(0, 0), new Vector2(1, 0), new Vector2(0, 1), new Vector2(1, 1) });

        var mr = backgroundGO.AddComponent<MeshRenderer>();
        backgroundGO.AddComponent<MeshFilter>().mesh = mesh;

        var material = new Material(Shader.Find("Unlit/Texture"));
        material.SetTexture("_MainTex", level.Image);

        mr.material = material;

        return backgroundGO;
    }

    private GameObject CreateTriangle(Level level, Triangle triangle, GameObject previous, int index)
    {
        var triangleGO = new GameObject();

        var mesh = new Mesh();
        mesh.vertices = new Vector3[] { new Vector3(triangle.A.x, triangle.A.y, -0.1f), new Vector3(triangle.B.x, triangle.B.y, -0.1f), new Vector3(triangle.C.x, triangle.C.y, -0.1f), new Vector3(triangle.C.x, triangle.C.y, -0.1f) };
        mesh.triangles = new int[] { 0, 2, 1, 1, 2, 3 };
        mesh.SetUVs(0, new List<Vector2> { new Vector2(0, 0), new Vector2(1, 0), new Vector2(0, 1), new Vector2(1, 1) });

        var mr = triangleGO.AddComponent<MeshRenderer>();
        triangleGO.AddComponent<MeshFilter>().mesh = mesh;

        var material = new Material(Shader.Find("Unlit/Color"));
        //material.SetTexture("_MainTex", level.Image);
        material.SetColor("_Color", level.TriangleColor);

        triangleGO.AddComponent<MeshCollider>().sharedMesh = mesh;
        var ci = triangleGO.AddComponent<ClickImage>();
        ci.ImageManager = this;
        ci.Previous = previous;
        ci.Index = index;
        ci.Image = level.Image;

        mr.material = material;

        return triangleGO;
    }

    internal void SendNext(int index)
    {
        if(LoadedLevels.Count> index + 1)
        {
            LoadedLevels[index + 1].SetActive(true);
        }
    }
}
