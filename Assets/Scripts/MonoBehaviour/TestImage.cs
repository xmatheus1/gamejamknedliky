﻿using Assets.Scripts.Scriptable;
using System.Collections.Generic;
using UnityEngine;

public class TestImage : MonoBehaviour
{
    public Vector3 CornerTopRight = new Vector3(7, 5, 0);
    public Vector3 CornerTopLeft = new Vector3(-7, 5, 0);
    public Vector3 CornerBottomRight = new Vector3(7, -5, 0);
    public Vector3 CornerBottomLeft = new Vector3(-7, -5, 0);

    private GameObject TriangleGO;

    public Level level;
    private void Start()
    {
        var backgroundGO = new GameObject();

        var mesh = new Mesh();
        mesh.vertices = new Vector3[] { CornerBottomLeft, CornerBottomRight, CornerTopLeft, CornerTopRight };
        mesh.triangles = new int[] { 0, 2, 1, 1, 2, 3 };
        mesh.SetUVs(0, new List<Vector2> { new Vector2(0, 0), new Vector2(1, 0), new Vector2(0, 1), new Vector2(1, 1) });

        var mr = backgroundGO.AddComponent<MeshRenderer>();
        backgroundGO.AddComponent<MeshFilter>().mesh = mesh;

        var material = new Material(Shader.Find("Unlit/Texture"));
        material.SetTexture("_MainTex", level.Image);

        mr.material = material;

        TriangleGO = new GameObject();

        mesh = new Mesh();
        mesh.vertices = new Vector3[] { new Vector3(level.Triangle.A.x, level.Triangle.A.y, -0.1f), new Vector3(level.Triangle.B.x, level.Triangle.B.y, -0.1f), new Vector3(level.Triangle.C.x, level.Triangle.C.y, -0.1f), new Vector3(level.Triangle.C.x, level.Triangle.C.y, -0.1f) };
        mesh.triangles = new int[] { 0, 2, 1, 1, 2, 3 };
        mesh.SetUVs(0, new List<Vector2> { new Vector2(0, 0), new Vector2(1, 0), new Vector2(0, 1), new Vector2(1, 1) });

        mr = TriangleGO.AddComponent<MeshRenderer>();
        TriangleGO.AddComponent<MeshFilter>().mesh = mesh;

        material = new Material(Shader.Find("Unlit/Color"));
        material.SetColor("_Color", level.TriangleColor);

        TriangleGO.AddComponent<MeshCollider>().sharedMesh = mesh;


        mr.material = material;

    }

    private void Update()
    {
        var mesh = TriangleGO.GetComponent<MeshFilter>().mesh;

        var newVertices = new Vector3[]
        {
            new Vector3(level.Triangle.A.x, level.Triangle.A.y, -0.1f), new Vector3(level.Triangle.B.x, level.Triangle.B.y, -0.1f), new Vector3(level.Triangle.C.x, level.Triangle.C.y, -0.1f), new Vector3(level.Triangle.C.x, level.Triangle.C.y, -0.1f)
        };

        mesh.vertices = newVertices;

        var mr = TriangleGO.GetComponent<MeshRenderer>();
        mr.sharedMaterial.SetColor("_Color", level.TriangleColor);
    }

}
