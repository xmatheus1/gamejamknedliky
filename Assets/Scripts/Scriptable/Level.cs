﻿using UnityEngine;

namespace Assets.Scripts.Scriptable
{
    [CreateAssetMenu(menuName = "Level")]
    public class Level : ScriptableObject
    {
        public Texture Image;
        public Triangle Triangle;
        public Color TriangleColor;
    }
}
