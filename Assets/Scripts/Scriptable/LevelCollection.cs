﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Scriptable
{
    [CreateAssetMenu(menuName = "Level Collection")]
    public class LevelCollection : ScriptableObject
    {
        public List<Level> Levels;
    }
}
